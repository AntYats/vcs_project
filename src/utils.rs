use std::collections::HashMap;
use hex_literal::hex;
use sha256;
use serde::Deserialize;

use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use crate::vcs_state_manager::{CommitData, StateData};

pub fn generate_hash(path: String) -> String {
    let bytes = std::fs::read(path.clone()).unwrap();
    let hasher = sha256::digest_bytes(&bytes);

    hasher
}

pub fn generate_branch_list_hashmap_from_json(path: String) -> HashMap<String, String> {
    println!("{}", path);
    let file = File::open(path).expect("NO FILE");
    let reader = BufReader::new(file);
    //

    // Read the JSON contents of the file as an instance of `User`.
    let u: HashMap<String, String> = serde_json::from_reader(reader).expect("BAD JSON FILE");

    // Return the `User`.
    u
}

pub fn generate_commit_list_hashmap_from_json(path: String) -> HashMap<String, CommitData> {
    let file = File::open(path).expect("NO FILE");
    let reader = BufReader::new(file);

    // Read the JSON contents of the file as an instance of `User`.
    let u: HashMap<String, CommitData> = serde_json::from_reader(reader).expect("BAD JSON FILE");

    // Return the `User`.
    u
}

pub fn generate_current_state_hashmap_from_json(path: String) -> StateData {
    let file = File::open(path).expect("NO FILE");
    let reader = BufReader::new(file);

    // Read the JSON contents of the file as an instance of `User`.
    let u: StateData = serde_json::from_reader(reader).expect("BAD JSON FILE");

    // Return the `User`.
    u
}
