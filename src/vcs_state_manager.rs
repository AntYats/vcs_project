use std::borrow::Borrow;
use std::collections::HashMap;
use std::fs;
use chrono::DateTime;
use serde::{Deserialize, Serialize};
use serde_json::Result;
use chrono::prelude::Local;
use repo_file_manager::get_all_files_from_path_rep;
use crate::repo_file_manager;
use crate::repo_file_manager::{ChangeOfFileState, copy_dir_all, create_commit, FileStatus, get_changed_files, remove_dir_all};
use std::{env, io, path::PathBuf, process};
use std::error::Error;
use std::fs::{File, read};
use std::io::BufReader;
use std::path::Path;
use crate::utils::{generate_branch_list_hashmap_from_json, generate_commit_list_hashmap_from_json, generate_current_state_hashmap_from_json};

pub struct StateManager {
    pub branch_list: HashMap<String, String>,
    pub commit_list: HashMap<String, CommitData>,
    pub current_state: StateData,
    pub path: String,
}

pub fn get_current_path() -> String {
    let exe = env::current_dir().unwrap();

    exe.into_os_string().into_string().unwrap()
}

impl StateManager {
    pub fn new(_path: String) -> Self {
        Self {
            branch_list: generate_branch_list_hashmap_from_json(_path.clone() + "\\.vcs" + "\\branch_list.json"),
            commit_list: generate_commit_list_hashmap_from_json(_path.clone() + "\\.vcs" + "\\commit_list.json"),
            current_state: generate_current_state_hashmap_from_json(_path.clone() + "\\.vcs" + "\\state.json"),
            path: _path.clone(),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct BranchData {
    name_of_branch: String,
    last_commit: String,
}

impl BranchData {
    pub fn new(_name_of_branch: String, _last_commit: String) -> Self {
        Self {
            name_of_branch: _name_of_branch,
            last_commit: _last_commit,
        }
    }
}


#[derive(Serialize, Deserialize)]
pub struct CommitData {
    pub(crate) time: DateTime<Local>,
    pub(crate) message: String,
    pub(crate) parent: String,
    pub(crate) changed_files: Vec<FileStatus>,
}

impl CommitData {
    pub fn new(_time: DateTime<Local>, _message: String, _parent: String, _changed_files: Vec<FileStatus>) -> Self {
        Self {
            time: _time,
            message: _message,
            parent: _parent,
            changed_files: _changed_files,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct StateData {
    pub(crate) current_branch: String,
    pub(crate) current_commit: String,
}

impl StateData {
    pub fn new(_current_branch: String, _current_commit: String) -> Self {
        Self {
            current_branch: _current_branch,
            current_commit: _current_commit,
        }
    }
}


pub fn init(path: String) {
    fs::write(get_current_path() + "/path.txt", path.clone()).expect("Unable to create file path");

    fs::create_dir(path.clone() + "/.vcs").expect("Unable to create .vcs directory");
    fs::create_dir(path.clone() + "/.vcs" + "/commits").expect("Unable to create commits directory");
    fs::create_dir(path.clone() + "/.vcs" + "/commits" + "/initial_commit").expect("Unable to create commits directory");


    let initial_commit = CommitData::new(Local::now(), "Initial_Commit".to_string(), "".to_string(), Vec::new());

    let mut branch_list: HashMap<String, String> = HashMap::new();
    branch_list.insert("main".to_string(), "initial_commit".to_string());

    let mut commit_list: HashMap<String, CommitData> = HashMap::new();
    commit_list.insert("initial_commit".to_string(), initial_commit);

    let mut current_state: StateData = StateData {
        current_branch: "main".to_string(),
        current_commit: "initial_commit".to_string(),
    };

    fs::write(path.clone() + "/.vcs" + "/branch_list.json", serde_json::to_string(&branch_list).unwrap()).expect("Unable to create branch_list.json");
    fs::write(path.clone() + "/.vcs" + "/commit_list.json", serde_json::to_string(&commit_list).unwrap()).expect("Unable to create commit_list.json");
    fs::write(path.clone() + "/.vcs" + "/state.json", serde_json::to_string(&current_state).unwrap()).expect("Unable to create state.json");

    println!("Initialized VCS repository in {}", path.clone());
    println!("Created commit: ");
    println!("[master, initial_commit] Initial commit");
}

pub fn log(state_manager: &mut StateManager) {
    for (key, value) in state_manager.commit_list.iter() {
        println!("commit {}", key);
        println!("Date: {}", value.time);
        println!("Message: {}", value.message);
        println!("Changes:");
        for file in value.changed_files.iter() {
            match file.status {
                ChangeOfFileState::Added => {
                    println!("added: {}", file.name);
                }
                ChangeOfFileState::Modified => {
                    println!("modified: {}", file.name);
                }
                ChangeOfFileState::Deleted => {
                    println!("deleted: {}", file.name);
                }
            }
        }
    }
}

pub fn status(state_manager: &mut StateManager) {
    let changes = get_changed_files(state_manager.path.to_string().clone(), state_manager.path.to_string().clone() + "\\.vcs" + "\\commits\\" + state_manager.current_state.current_commit.as_str());
    if changes.is_empty() {
        println!("No changes to be committed");
    } else {
        println!("Changes:");
        for file in changes.iter() {
            match file.status {
                ChangeOfFileState::Added => {
                    println!("added: {}", file.name);
                }
                ChangeOfFileState::Modified => {
                    println!("modified: {}", file.name);
                }
                ChangeOfFileState::Deleted => {
                    println!("deleted: {}", file.name);
                }
            }
        }
    }
}

pub fn new_branch(state_manager: &mut StateManager, branch_name: String) {
    state_manager.branch_list.insert(branch_name.clone(), state_manager.current_state.current_commit.clone());
    fs::write(state_manager.path.clone() + "/.vcs" + "/branch_list.json", serde_json::to_string(&state_manager.branch_list).unwrap()).expect("Unable to create state.json");
}

pub fn change_branch(state_manager: &mut StateManager, branch_name: String) {
    state_manager.current_state.current_branch = branch_name.clone();
    let current_commit = state_manager.branch_list.get(&branch_name.clone()).unwrap();
    state_manager.current_state.current_commit = current_commit.to_string();
    fs::write(state_manager.path.clone() + "/.vcs" + "/state.json", serde_json::to_string(&state_manager.current_state).unwrap()).expect("Unable to create state.json");
}

pub fn change_commit(state_manager: &mut StateManager, commit_name: String) {
    remove_dir_all(state_manager.path.clone(), state_manager.path.clone());
    copy_dir_all(state_manager.path.clone() + "\\.vcs" + "\\commits" + "\\" + &commit_name.clone(), state_manager.path.clone()).expect("Panic!");
    state_manager.current_state.current_commit = commit_name.clone();
    fs::write(state_manager.path.clone() + "/.vcs" + "/state.json", serde_json::to_string(&state_manager.current_state).unwrap()).expect("Unable to create state.json");
}