use crate::StateManager;
use crate::vcs_state_manager::init;

pub fn Init(path: String) {
    init(path);
}