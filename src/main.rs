mod command_parser;
mod vcs_state_manager;
mod commands;
mod repo_file_manager;
mod utils;

use clap::{Args, Parser};
use command_parser::CommandParser;
use crate::command_parser::Command;
use commands::init::Init;
use std::collections::HashMap;
use std::fs;
use crate::repo_file_manager::{ChangeOfFileState, create_commit, get_changed_files};
use crate::vcs_state_manager::{change_branch, change_commit, get_current_path, init, log, new_branch, StateManager, status};
use crate::utils::generate_hash;
use clap::{Arg, ArgAction};

const ARG_NUMBER: &str = "number";
const DEFAULT_NUMBER: usize = 10;

fn main() {
    let mut parser = CommandParser::parse();

    match &parser.command {
        Command::Init { path } => {
            init(path.clone())
        }
        Command::Commit { message } => {
            let path= fs::read_to_string(get_current_path() + "\\path.txt").expect("NO FILE PATH");
            let mut state_manager = StateManager::new(path);
            create_commit(message.clone(), &mut state_manager);
        }
        Command::NewBranch { name } => {
            let path= fs::read_to_string(get_current_path() + "\\path.txt").expect("NO FILE PATH");
            let mut state_manager = StateManager::new(path);
            new_branch(&mut state_manager, name.to_string());
        }
        Command::Jump { branch, commit } => {
            if !branch.is_none() {
                let path= fs::read_to_string(get_current_path() + "\\path.txt").expect("NO FILE PATH");
                let mut state_manager = StateManager::new(path);
                change_branch(&mut state_manager, branch.as_ref().unwrap().to_string());
            };

            if !commit.is_none() {
                let path= fs::read_to_string(get_current_path() + "\\path.txt").expect("NO FILE PATH");
                let mut state_manager = StateManager::new(path);
                change_commit(&mut state_manager, commit.as_ref().unwrap().to_string());
            }
        }
        Command::Log => {
            let mut state_manager = StateManager::new(fs::read_to_string(get_current_path() + "\\path.txt").expect("NO FILE PATH"));
            log(&mut state_manager);
        }
        Command::Status => {
            let mut state_manager = StateManager::new(fs::read_to_string(get_current_path()+ "\\path.txt").expect("NO FILE PATH"));
            status(&mut state_manager);
        }
        Command::Merge { branch } => {
            unimplemented!()
        }
    }
}
