use std::collections::HashMap;
use std::fs::File;
use chrono::{DateTime, Local};
use clap::command;
use walkdir::{DirEntry, WalkDir, FilterEntry};
use crate::StateManager;
use crate::utils::generate_hash;
use crate::vcs_state_manager::CommitData;
use random_string::generate;
use std::path::PathBuf;
use std::{fs, io};
use std::error::Error;
use serde::{Deserialize, Serialize};

fn is_hidden(entry: &DirEntry) -> bool {
    entry.file_name()
        .to_str()
        .map(|s| s.starts_with("."))
        .unwrap_or(false)
}

pub fn transform_path(path: String, default_path: String) {
    let def_path: Vec<_> = default_path.match_indices("\\").collect();
    let mut new_path: Vec<_> = path.match_indices("\\").collect();

    println!("{}", default_path);
    println!("{:?}", def_path);

    println!("{}", path);
    println!("{:?}", new_path);

    let index_of_begin_slice = def_path.last().unwrap().0;
    let mut array_index = 0;


    let start = new_path[def_path.len()].0;
    let end = new_path[def_path.len() + 2].0;

    let path_ = path.clone();
    let new_string: String = path_.chars().take(start).chain(path_.chars().skip(end)).collect();

    // println!("{}", default_path);
    // println!("{}", new_string);
}

pub fn get_all_files_from_path_rep(path: String, default_path: String) -> HashMap<String, String> {
    let mut files = HashMap::new();

    let walker = WalkDir::new(path).min_depth(1).into_iter();
    for entry in walker.filter_entry(|e| !is_hidden(e)) {
        let entry = entry.unwrap();

        let format_entry = entry.path().display().to_string();
        // transform_path(format_entry.clone(), default_path.clone());

        files.insert(format_entry.clone(), generate_hash(entry.path().display().to_string()));

        // println!("{}", entry.path().display());
        // println!("{}", files[&entry.path().display().to_string()]);
    }

    files
}

#[derive(Serialize, Deserialize)]
pub enum ChangeOfFileState {
    Modified,
    Added,
    Deleted,
}

#[derive(Serialize, Deserialize)]
pub struct FileStatus {
    pub(crate) name: String,
    pub(crate) status: ChangeOfFileState,
}

pub fn get_changed_files(working_path: String, commit_path: String) -> Vec<FileStatus> {
    let current_project_files = get_all_files_from_path_rep(working_path.clone(), working_path.clone());
    let previous_commit_files = get_all_files_from_path_rep(commit_path.clone(), working_path.clone());

    // println!("{}", current_project_files.len());
    // println!("{}", previous_commit_files.len());
    //
    let mut changed_files = Vec::new();

    for (key, value) in previous_commit_files.iter() {
        if current_project_files.contains_key(key) {
            if current_project_files[key] == value.clone() {
                changed_files.push(FileStatus {
                    name: key.to_string(),
                    status: ChangeOfFileState::Modified,
                });
            }
        } else {
            changed_files.push(FileStatus {
                name: key.to_string(),
                status: ChangeOfFileState::Deleted,
            });
        }
    }
    //
    for (key, value) in current_project_files.iter() {
        if !previous_commit_files.contains_key(key) {
            changed_files.push(FileStatus {
                name: key.to_string(),
                status: ChangeOfFileState::Added,
            });
        }
    }
    //
    // println!("{}", changed_files.len());

    changed_files
}

pub fn copy_dir_all(src: String, dst: String) -> Result<(), Box<dyn Error>> {
    let in_dir = PathBuf::from(src);
    let out_dir = PathBuf::from(dst);

    for entry in WalkDir::new(&in_dir).into_iter().filter_entry(|e| !is_hidden(e)) {
        let entry = entry?;

        let from = entry.path();
        let to = out_dir.join(from.strip_prefix(&in_dir)?);
        println!("\tcopy {} => {}", from.display(), to.display());

        // create directories
        if entry.file_type().is_dir() {
            if let Err(e) = fs::create_dir(to) {
                match e.kind() {
                    io::ErrorKind::AlreadyExists => {}
                    _ => return Err(e.into()),
                }
            }
        }
        // copy files
        else if entry.file_type().is_file() {
            fs::copy(from, to)?;
        }
        // ignore the rest
        else {
            eprintln!("copy: ignored symlink {}", from.display());
        }
    }
    Ok(())
}

pub fn remove_dir_all(path: String, default_path: String) {
    let walker = WalkDir::new(path).into_iter();
    for entry in walker.filter_entry(|e| !is_hidden(e)) {
        let entry = entry.unwrap();

        let format_entry = entry.path().display().to_string();
        // transform_path(format_entry.clone(), default_path.clone());
        if format_entry != default_path.clone() {
            fs::remove_file(format_entry).expect("File wasn't deleted!");
        }
    }
}

pub fn create_commit(mes: String, mut state_manager: &mut StateManager) {
    let _changed_files = get_changed_files(state_manager.path.clone(), state_manager.path.clone() + "\\.vcs" + "\\commits" + "\\" + &state_manager.current_state.current_commit);

    let commit = CommitData {
        time: Local::now(),
        message: mes,
        parent: state_manager.current_state.current_commit.clone(),
        changed_files: _changed_files,
    };

    let commit_hash = random_string::generate(10, "abcdefghijklmnopqrstuvwxyz");
    state_manager.commit_list.insert(commit_hash.clone(), commit);
    state_manager.current_state.current_commit = commit_hash.clone();
    state_manager.branch_list.insert(state_manager.current_state.current_branch.clone(), commit_hash.clone());
    fs::create_dir(state_manager.path.clone() + "/.vcs" + "/commits" + "/" + &*state_manager.current_state.current_commit).expect("Unable to create commits directory");

    fs::write(state_manager.path.clone() + "/.vcs" + "/branch_list.json", serde_json::to_string(&state_manager.branch_list).unwrap()).expect("Unable to create branch_list.json");
    fs::write(state_manager.path.clone() + "/.vcs" + "/commit_list.json", serde_json::to_string(&state_manager.commit_list).unwrap()).expect("Unable to create commit_list.json");
    fs::write(state_manager.path.clone() + "/.vcs" + "/state.json", serde_json::to_string(&state_manager.current_state).unwrap()).expect("Unable to create state.json");

    copy_dir_all(state_manager.path.clone(), state_manager.path.clone() + "\\.vcs" + "\\commits" + "\\" + &state_manager.current_state.current_commit).expect("Panic!");
}